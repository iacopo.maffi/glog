# glog

Traduzione italiana di Glog - many rats on a stick edition

# Fonte

Il PDF originale si trova [qui](https://drive.google.com/file/d/1wOAkBOCUSjnthMEnIsPVT1LSOCQzd88j/view)

# Come contribuire

Questa traduzione è solo testuale (più le tabelle) per il typesetting e le illustrazioni ci inventeremo qualcosa di figo andando avanti.

Innanzitutto, serve avere un po' di dimestichezza con il [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet), in secondo luogo, se non avete padronanza
di git, usate l'IDE web di Gitlab, che è meglio. *Per amor del cielo e della nostra sanità mentale, usate i branch, mi occuperò io di fare i merge su master*

Se avete dubbi o proposte che non riuscite a gestire, ci sono gli [issue](https://gitlab.com/frollo/glog/-/issues).

Cercate di tenere la struttura dei file che c'è adesso, in modo da non pestarci troppo i piedi.

Se non avete accesso, chiedetelo pure.

# Pipeline

Ogni vostro cambiamento fa scattare la pipeline che, sperabilmente, ci produce un PDF finito con tutto quello che abbiamo tradotto impaginato assieme. Se la pipeline va in errore non vi allarmate. Se vi scrivo dopo che la pipeline è andata in errore, *allarmatevi*.
